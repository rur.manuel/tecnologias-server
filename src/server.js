import express from 'express'
import { json, urlencoded } from 'body-parser'
import morgan from 'morgan'
import config from './config'
import cors from 'cors'
import { signup, signin, protect } from './utils/auth'
import { connect } from './utils/db'
import { connect as connectWs } from './utils/socket-server'

import userRouter from './resources/user/user.router'
import deviceRouter from './resources/device/device.router'
import raspberryRouter from './resources/raspberry/raspberry.router'

export const app = express()

app.disable('x-powered-by')

app.use(cors())
app.use(json())
app.use(urlencoded({ extended: true }))
app.use(morgan('dev'))

app.post('/signup', signup)
app.post('/signin', signin)

app.use('/api', protect)
app.use('/api/user', userRouter)
app.use('/api/device', deviceRouter)

export const start = async () => {
  try {
    const db = await connect()
    let server = app.listen(config.port, () => {
      console.log(`REST API on http://localhost:${config.port}/api`)
    })
    connectWs(server, raspberryRouter)
  } catch (e) {
    console.error(e)
  }
}

export const config = {
  secrets: {
    jwt: 'learneverything',
    jwtExp: '1d'
  },
  dbUrl: 'mongodb://localhost:27017/securydoor'
}

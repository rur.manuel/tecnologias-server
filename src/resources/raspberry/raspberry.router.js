import controllers from './raspberry.controller'

const routes = {
  'register': controllers.registerRaspberry,
  'unregister': controllers.unregisterRaspberry
}

function routeActions(data, socket, socketServer) {
  if (!data.action) {
    return
  }

  const route = routes[data.action]
  if (route) {
    route(data, socket, socketServer)
  }
}

export function bind(socket, socketServer) {
  console.log('New socket connection')
  socket.on('message', (data) => {
    try {
      if (['dev', 'development'].includes(process.env.NODE_ENV)) {
        console.log(data)
      }
      data = JSON.parse(data)

      routeActions(data, socket, socketServer)
    } catch (err) {
      console.log(err)
    }
  })
  
  socket.on(
    'close',
    () => controllers.unregisterRaspberry({}, socket, socketServer)
  )

  socket.on(
    'error', 
    (error) => { console.warn('socketError',  error)}
  )
}

export default bind

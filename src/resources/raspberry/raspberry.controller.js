import { Device } from '../device/device.model'

async function registerRaspberry(data, socket, serverSocket) {
  try {
    const { beaconUuid, code } = data
    if (!beaconUuid) {
      throw { message: 'Device not found' }
    }

    let device = await Device.findOne({ beaconUuid })

    if (!device) {
      device = await Device.create({ beaconUuid, code, status: 1 })
    } else {
      device.status = 1
      await device.save()
    }

    const deviceId = device._id
    socket.deviceId = deviceId
    serverSocket.connections[deviceId] = socket
    console.log('registered',  device._id)
    socket.send(JSON.stringify({ device, action: 'register', success: true }))
  } catch (err) {
    console.warn(err)
    socket.send(JSON.stringify({ error: err.message }))
  }
}

async function unregisterRaspberry(_, socket, socketSocket) {
  try {
    const deviceId = socket.deviceId
    if (deviceId) {
      console.log('closed', deviceId)
      delete socketSocket.connections[deviceId]
      // console.log('device', await Device.findOne({ _id: deviceId }))
      await Device.updateOne({ _id: deviceId }, { status: 0 })
    } else {
      console.log('closed unregistered')
    }
  } catch (err) {
    console.warn(err)
  }
}

export default {
  registerRaspberry,
  unregisterRaspberry
};

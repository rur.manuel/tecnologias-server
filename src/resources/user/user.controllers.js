import { crudControllers } from '../../utils/crud'
import { User } from './user.model'

export const me = (req, res) => {
  res.status(200).json({ data: req.user })
}

export const updateMe = async (req, res) => {
  try {
    const { name, email, password } = req.body
    const user = await User.findByIdAndUpdate(
      req.user._id,
      {
        name: name || req.user.name,
        email: email || req.user.email,
        password: password || req.user.password},
      { new: true }
    ).lean()
    .exec()

    res.status(200).json({ data: user })
  } catch (e) {
    console.error(e)
    res.status(400).end()
  }
}

const crud = crudControllers(User)
export default {
  find: crud.find,
  me,
  updateMe
}

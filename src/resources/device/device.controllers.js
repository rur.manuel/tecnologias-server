import { crudControllers } from '../../utils/crud'
import { Device } from './device.model'
import { User } from '../user/user.model'
import { getServer } from '../../utils/socket-server'

export const me = (req, res) => {
  res.status(200).json({ data: req.user })
}

export const register = async (req, res) => {
  try {
    const { code, beaconUuid } = req.body
    const userId = req.user._id
    
    if (!(code && beaconUuid)) {
      throw { message: 'required beaconUuuid and code'}
    }

    let device = await Device.findOne({ code, beaconUuid })
    
    if (!device) {
      throw { statusCode: 404, message: 'Device not found' }
    }

    if (!device.createdBy) {
      device.createdBy = req.user
      device = await device.save()
    } else if (!device.createdBy.equals(userId)) {
      throw { statusCode: 403, message: 'Device already registered' }
    }

    res.send(device)
  } catch (e) {
    console.error(e)
    res.status(e.statusCode || 400)
      .send({message: e.message || 'Uknown error'})
  }
}

export const open = async (req, res) => {
  try {
    const { beaconUuid, code } = req.body
    const userId = req.user?._id

    if (!beaconUuid) {
      throw { message: 'required beaconUuuid' }
    }

    // Busca el dispositivo ya sea por codigo 
    const device = await Device.findOne({
      $and: [
        { beaconUuid },
        {
          $or: [
            { code },
            { createdBy: userId },
          ]
        }
      ]
    })

    if (!device) {
      throw { statusCode: 404,  message: 'device not found'}
    }

    const socketServer = getServer()
    const socket = socketServer?.connections[device._id]
    if (!socket) {
      throw { message: 'device is not connected' }
    }

    socket.send(JSON.stringify({ action: 'open' }))
    res.send({ success: true })
  } catch (e) {
    console.error(e)
    res.status(e.statusCode || 400)
      .send({message: e.message || 'Uknown error'})
  }
}

export const updateCode = async (req, res) => {
  try {
    const { code, beaconUuid } = req.body
    const userId = req.user._id

    if (!(code && beaconUuid) && code.length !== 4) {
      throw { message: 'Invalid data' }
    }
  
    const device = await Device.findOne({ beaconUuid })
  
    if (!device) {
      throw { statusCode: 404, message: 'Device not found' }
    }

    if (!device.createdBy.equals(userId)) {
      throw { statusCode: 403, message: 'forbidden' }
    }

    device.code = code
    await device.save()
    res.status(200).send({ success: true })
  } catch (e) {
    console.warn(e)
    res.status(e.statusCode || 400)
      .send({message: e.message || 'Uknown error'})
  }
}

export const findOwn = async (req, res) => {
  try {
    const userId = req.user._id

    const devices = await Device.find({ createdBy: userId })
    res.status(200).send({ data: devices })
  } catch (e) {
    console.warn(e)
    res.status(e.statusCode || 400)
      .send({message: e.message || 'Uknown error'})
  }
}

const crud = crudControllers(Device)
export default {
  findOwn,
  updateCode,
  register,
  open,
  find: crud.find,
}

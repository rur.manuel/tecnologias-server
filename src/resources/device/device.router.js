import { Router } from 'express'
import controllers from './device.controllers'

const router = Router()

// /api/item
router
  .route('/')
  .get(controllers.find)

router.get('/owned', controllers.findOwn)
router.post('/register', controllers.register)
router.post('/open', controllers.open)
router.post('/update_code', controllers.updateCode)


export default router

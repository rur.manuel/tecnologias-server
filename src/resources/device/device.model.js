import mongoose from 'mongoose'
import { toUpper } from 'lodash'

const itemSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
      maxlength: 50,
      default: 'New device'
    },
    beaconUuid: {
      type:String,
      set: toUpper,
      maxlength: 50,
      required: true,
    },
    status: {
      type: Number,
      default: 0
    },
    code: {
      type: String,
      required: true,
      maxlength: 4
    },
    createdBy: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'user',
    },
  },
  { timestamps: true }
)

itemSchema.index({ beaconUuid: 1 }, { unique: true })

export const Device = mongoose.model('device', itemSchema)

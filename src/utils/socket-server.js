import WebSocket from 'ws'

let socketServer = null

export function connect(server, bindController) {
  socketServer = new WebSocket.Server({server, path: '/raspberry'})
  socketServer.connections = {}
  
  socketServer.on(
    'connection',
    (socket) => bindController(socket, socketServer)
  )
}

export function getServer() {
  return socketServer
}
